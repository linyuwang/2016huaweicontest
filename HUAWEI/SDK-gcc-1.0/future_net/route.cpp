#include "route.h"
#include "lib_record.h"
#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <sys/time.h>
#include <time.h>
#define maxn 601
int monteDepth = 300;
int monteTimes = 200;
using namespace std;
int search(int u,int cost);
void simpleGraph(int n);
void montecalo(int n);
vector<int> ans,redis;
int check[maxn]; //是否需要覆盖的点
int over[maxn]; //是否在搜索的路径上
int covernum;   //已覆盖的点数
int maxCost;
int flag[maxn];
int valeva[maxn]; //估值函数
struct Edge{
	int id,u,c;
	Edge(){}
	Edge(int id,int u,int c):id(id),u(u),c(c){}
};
int comp(Edge a,Edge b){
    if(check[a.u] == check[b.u])
        return valeva[a.u] > valeva[b.u];
    else
        return check[a.u] > check[b.u];
}
vector<Edge> edge[maxn];

//你要完成的功能总入口
int getSystemTime(){
   struct timeval tv;
   gettimeofday(&tv,NULL);
   return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

//========
vector<int> needV;
int startV,endV;
int mp[maxn][maxn],linkId[maxn][maxn];
clockid_t startTime,endTime;
void search_route(char *topo[5000], int edge_num, char *demand)
{
    startTime = getSystemTime();
    //cout<<startTime<<endl;
    srand(time(0));
	//========================================解析边============================
	int len = strlen(demand),n = 0;
	memset(mp,-1,sizeof(mp));
	int mess[4];
	for(int i = 0;i < edge_num; i++){
		int j = 0;
		len = strlen(topo[i]);
		memset(mess,0,sizeof(mess));
		for(int k = 0;k < len; k++){
			if(topo[i][k] > '9' || topo[i][k] < '0'){
				j++;
				continue;
			}
			mess[j] = mess[j]*10+topo[i][k]-'0';
			n = max(n,mess[1]);
			n = max(n,mess[2]);
		}
		if(mp[mess[1]][mess[2]] == -1){
			mp[mess[1]][mess[2]] = mess[3];
			linkId[mess[1]][mess[2]] = mess[0];
		}
		if(mp[mess[1]][mess[2]] > mess[3]){
			mp[mess[1]][mess[2]] = mess[3];
			linkId[mess[1]][mess[2]] = mess[0];
		}
		//for(int k = 0;k < 4; k++)
		//	cout<<mess[k]<<" ";
		//cout<<endl;
	}

	//=======================================解析出发结束点,必须访问的集合
	len = strlen(demand);
	int po = 0;
	memset(check,0,sizeof(check));
	startV = endV = 0;
	while(demand[po] != ','){
		startV = startV * 10 + demand[po] - '0';
		po++;
	}
	po++;
	while(demand[po] != ','){
		endV = endV * 10 + demand[po] - '0';
		po++;
	}
	po++;
	mess[0] = 0;
	while(po < len){
		if(demand[po] > '9' || demand[po] < '0'){
			needV.push_back(mess[0]);
			po++;
			check[mess[0]] = 1;
			mess[0] = 0;
			continue;
		}
		mess[0] = mess[0] * 10 + demand[po++] - '0';
	}
	//cout<<startV<<" "<<endV<<" "<<endl;
	//for(int i = 0;i < needV.size(); i++){
	//	cout<<needV[i]<<endl;
	//}
	//=======================================搜索准备
	monteDepth = n;
	monteTimes = 200;
	ans.clear(), redis.clear();
	maxCost = 10000000;
	covernum = needV.size();
	memset(over,0,sizeof(over));
	simpleGraph(n);
    //======================================建图
    for(int i = 0;i <= n; i++){
		for(int j = 0;j <= n; j++){
			if(i == j) continue;
			if(mp[i][j] == -1) continue;
			edge[i].push_back(Edge(linkId[i][j],j,mp[i][j]));
		}

	}
    montecalo(n);
    for(int i = 0;i <= n; i++)
        sort(edge[i].begin(),edge[i].end(),comp);
    endTime = clock();
    //cout<<"time:"<<(endTime - startTime) /1000<<endl;
	search(startV,0);
    //unsigned short result[] = {2, 6, 3};//示例中的一个解
    for (int i = 0; i < ans.size() ; i++)
        record_result(ans[i]);
}

int search(int u,int cost){
    endTime = getSystemTime();
    if((endTime - startTime)  > 9800) return -1;
	if(cost >= maxCost)
		return 0;
    //cout<<u<<" "<<cost<<" "<<maxCost<<endl;
	if(u == endV && covernum == 0){
		ans.clear();
		maxCost = cost;
		for(int i = 0;i < redis.size(); i++)
			ans.push_back(redis[i]);
		//cout<<cost<<endl;
		//for(int i = 0;i < redis.size(); i++)
			//cout<<redis[i]<<"|";
		//cout<<endl;
		//cout<<"time:"<<(endTime - startTime) /1000<<endl;
		return 0;
	}
	if(u == endV)
        return 0;
	over[u] = 1;
	for(int i = 0;i < edge[u].size() ;i++){
		int v = edge[u][i].u;
		if(over[v] == 1) continue;
		redis.push_back(edge[u][i].id);
		if(check[v])covernum--;
		if(search(v,cost+edge[u][i].c) == -1)
            return -1;
		redis.pop_back();
		if(check[v]) covernum++;
	}

	over[u] = 0;
	return 0;
}



void simpleGraph(int n){
	memset(flag,0,sizeof(flag));
	flag[startV] = 1;
	vector<int> pass;
	pass.push_back(startV);
	while(pass.size() > 0){
		int u = pass[pass.size()-1];
		pass.pop_back();
		for(int i = 0;i <= n; i++){
			if(mp[u][i] != -1){
				if(flag[i] == 0){
					flag[i] = 1;
					pass.push_back(i);
				}
			}
		}
	}
	for(int i = 0;i <= n; i++)
		if(flag[i] == 0)
			over[i] = 1;

	memset(flag,0,sizeof(flag));
	flag[endV] = 1;
	pass.clear();
	pass.push_back(endV);
	while(pass.size() > 0){
		int u = pass[pass.size()-1];
		pass.pop_back();
		for(int i = 0;i <= n; i++){
			if(mp[i][u] != -1){
				if(flag[i] == 0){
					flag[i] = 1;
					pass.push_back(i);
				}
			}
		}
	}
	for(int i = 0;i <= n; i++)
		if(flag[i] == 0)
			over[i] = 1;
	int x = 0;
	for(int i = 0;i <= n; i++){
		if(over[i]){
			for(int j = 0;j <= n; j++)
				mp[i][j] = mp[j][i] = -1;
		}
		x += over[i];
	}
	//cout<<"edge cut:"<<x<<endl;
}

int simulation(int u,int dep){
    int ans = 0;
    if(check[u]) ans++;
       if(dep == monteDepth)
         return 0;
    flag[u] = 1;
    for(int i = 0;i < 2; i++){
        if(edge[u].size() == 0) break;
        int v = edge[u][rand()%edge[u].size()].u;
        if(flag[v]) continue;
        ans += simulation(v,dep+1);
        break;
    }

    flag[u] = 0;
    return ans;
}

void montecalo(int n){
	memset(valeva,0,sizeof(valeva));
	memset(flag,0,sizeof(flag));
    for(int i = 0;i <= n; i++){
        flag[i] = 1;
        for(int j = 0;j < monteTimes;j++)
            valeva[i] += simulation(i,0);
        flag[i] = 0;
    }
    ////cout<<"monte Calo "<<endl;
    //for(int i = 0;i <= n; i++){
    //    cout<<i<<":"<<valeva[i]<<endl;
    //}
}




















