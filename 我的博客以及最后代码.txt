http://blog.csdn.net/firenet1/article/details/51417314
这里是我的博客，写了相关的信息。
由于我的硬盘坏了，这里的内容都是从备份的虚拟机里拷贝的，因此可能不完全


#include "route.h"
#include "lib_record.h"
#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include <set>
#include <fstream>
#include <bitset>
using namespace std;

namespace GDRetop{
    #define maxV 2001
    #define maxE 40001
    #define maxEdgeCost 200000
    #define maxNeedPointNum 105
    int getSystemTime();
    void stringAnalysis(char *word,vector<int>& result);
    void getBestAnser(vector<vector<int> >&a,vector<vector<int> >&b);
    //=================================================================================
    vector<int> sourceEdge[maxE];
    vector<int> needVertex[2];
    bitset<maxV> occurVertex; //出现过的点
    vector<vector<int> >ansPathOne,ansPathTwo;
    int mark =0,markM[maxV],edgeNum,vertexNum;
    struct Edge{
        int u,v,c,id,dist;
        Edge(){}
        Edge(vector<int> e){
            id = e[0];
            u = e[1];
            v = e[2];
            c = e[3];
        }
        bool operator < (const Edge& b)const{
            return c < b.c;
        }
    };
    struct WholeEdge{
        int u,v;
        int c[2];
        vector<Edge> edge;
        WholeEdge(){}
    };
    vector<WholeEdge> wholeEdge;
    vector<int> nodeEdge[maxV];
    class Search{
        public:
            struct Node{
                int id,v;
                Node(){}
                Node(int _id,int _v):id(_id),v(_v){}
            };
            vector<vector<int> > resultPath;
            bitset<maxE> usefulEdge;          //边是否有效
            bitset<maxV> needPoint;           //是否需要的点
            bitset<maxE> findForward;                 //判断被访问过的边 前向
            bitset<maxNeedPointNum> dpForward[maxE];  //dp 用于判定访问了哪些点,前向
            bitset<maxE> findBack;                    //判断被访问过的边 后向
            bitset<maxNeedPointNum> dpBack[maxE];     //dp 用于判定访问了哪些点,后向
            bitset<maxNeedPointNum> checkNeedPoint;

            vector<Node> forwardEdgeId[maxV]; //前向路径
            vector<Node> backEdgeId[maxV];    //后向路径
            int vertexNumber,que[maxV],mapToId[maxV];
            bitset<maxV> checkVertex;         //临时判定点变量
            bitset<maxE> checkEdge;           //临时判定边变量

            int check[maxV];
            int startTime,endTime,TimeLong;
            int needVnum,covernum,startV,endV,maxCost;
            int queue[maxV],from[maxV];
            int maxDist,type;
            struct searchEdge{
                int u,v,c,id,dist;
                searchEdge(){}
                searchEdge(WholeEdge & we,int _id){
                    u = we.u;
                    v = we.v;
                    id = _id;
                    c = we.c[0];
                }
                bool operator < (const searchEdge& b)const{
                    if(dist == b.dist) return c < b.c;
                    return dist < b.dist;
                }
            };
            vector<searchEdge> edgeTo[maxV];
            vector<int> needVertex;
            vector<int> route;
            int depth[maxV];
            //初始化地图
            void initGraph(vector<int> &need,int timeLimit){
                int initTime = getSystemTime();
                vertexNumber = vertexNum;
                needVertex = need;
                needPoint.reset();
                checkNeedPoint.reset();
                for(int i = 0;i < needVertex.size();i++){
                    needPoint.set(needVertex[i]);
                    checkNeedPoint.set(i);
                }
                needVnum = needVertex.size();
                memset(mapToId,-1,sizeof(mapToId));
                for(int i = 0;i < needVertex.size();i++)
                    mapToId[needVertex[i]] = i;

                usefulEdge.reset();
                for(int i = 0;i < wholeEdge.size();i++){
                    usefulEdge.set(i);
                }
                maxDist = maxEdgeCost;
                startV = needVertex[0];
                endV = needVertex[1];
                initGraph();
                int ans = 0,res = wholeEdge.size();
                vector<int> q;
                int tail = 0;
                checkVertex.reset();
                checkVertex.set(startV);
                q.push_back(startV);
                depth[startV] = 5;
                while(tail < q.size()){
                    int u = q[tail++];
                    for(int i = 0;i < forwardEdgeId[u].size();i++){
                        int v = forwardEdgeId[u][i].v;
                        if(checkVertex.test(v) == false){
                            checkVertex.set(v);
                            q.push_back(v);
                            depth[v] = depth[u]-1;
                        }
                    }
                }

                for(int i = 1;i < q.size();i++){
                    if(occurVertex.test(i) ){
                        edgeCut(q[i]);
                        //initGraph();
                        ans = usefulEdge.count();
//                        if(res - ans > 200){
//                            res = ans;
//                            initGraph();
//                        }
//                        for(int j = 0;j < wholeEdge.size();j++){
//                            if(usefulEdge.test(j)) ans++;
//                        }
                        //cout<<q[i]<<" "<<res - usefulEdge.count()<<endl;
                        res = usefulEdge.count();
                        if(getSystemTime() - initTime > timeLimit) goto aaa ;
                        //if(ans != res) {cout<<"edge less: "<<i<<" "<<res-ans<<" "<<ans<<endl; res =ans;}
                   }
                }
                aaa:
                for(int i = 0;i <= vertexNumber; i++){
                    edgeTo[i].clear();
                }
                for(int i = 0;i < wholeEdge.size();i++){
                    if(usefulEdge.test(i)){
                        int u = wholeEdge[i].u;
                        edgeTo[u].push_back(searchEdge(wholeEdge[i],i));
                    }
                }
            }
            //构建边
            void initGraph(){
                for(int i = 0;i <= vertexNumber; i++){
                    forwardEdgeId[i].clear();
                    backEdgeId[i].clear();
                }
                for(int i = 0;i < wholeEdge.size();i++){
                    if(usefulEdge.test(i)){
                        forwardEdgeId[wholeEdge[i].u].push_back(Node(i,wholeEdge[i].v));
                        backEdgeId[wholeEdge[i].v].push_back(Node(i,wholeEdge[i].u));
                    }
                }
            }
            //割边
            void edgeCut(int u){
                findForward = findBack = usefulEdge;
                static bitset<maxNeedPointNum> begin;
                begin.reset();
                if(needPoint.test(u)){
                    begin.set(mapToId[u]);
                }
                countVisitNeed(startV,u,forwardEdgeId,begin);
                checkEdge = usefulEdge;
                //if(begin == checkNeedPoint) return;
                memset(check,0,sizeof(check));
                tarstack.clear();
                tarMark = 0,newid  = 0;
                for(int i = 0;i < forwardEdgeId[u].size();i++){
                    checkEdge.reset(forwardEdgeId[u][i].id);
                    if(check[forwardEdgeId[u][i].v] == 0 && usefulEdge.test(forwardEdgeId[u][i].id)){
                        tarJan(forwardEdgeId[u][i].v,u,forwardEdgeId);
                    }
                }

                checkVertex.reset();
                for(int i = 0;i < forwardEdgeId[u].size(); i++){
                    if(usefulEdge.test(forwardEdgeId[u][i].id))
                        updateEdgestate(tarbelong[forwardEdgeId[u][i].v],dpForward,forwardEdgeId);
                }
                for(int i = 0;i <= vertexNum ;i++)
                    tarbelongback[i] = tarbelong[i];

                memset(check,0,sizeof(check));
                tarstack.clear();
                tarMark = 0,newid  = 0;
                for(int i = 0;i < backEdgeId[u].size();i++){
                    checkEdge.reset(backEdgeId[u][i].id);
                    if(check[backEdgeId[u][i].v] == 0 && usefulEdge.test(backEdgeId[u][i].id)){
                        tarJan(backEdgeId[u][i].v,u,backEdgeId);
                    }
                }
                checkVertex.reset();
                for(int i = 0;i < backEdgeId[u].size(); i++){
                    if(usefulEdge.test(backEdgeId[u][i].id))
                        updateEdgestate(tarbelong[backEdgeId[u][i].v],dpBack,backEdgeId);
                }

                if(u != startV && begin != checkNeedPoint){
                    usefulEdge ^= checkEdge;
                }
                //return ;
                for(int i = 0;i < forwardEdgeId[u].size(); i++){
                    int flag = 1;
                    if(usefulEdge.test(forwardEdgeId[u][i].id) == false)
                        continue;
                    int ux = tarbelongback[forwardEdgeId[u][i].v];
                    if(needPoint[u])
                        dpForward[ux].set(mapToId[u]);
                    if(dpForward[ux] == checkNeedPoint) continue;
                    for(int j = 0;j < backEdgeId[u].size() && flag; j++){
                        if(usefulEdge.test(backEdgeId[u][j].id) == false)
                            continue;
                        int vx = tarbelong[backEdgeId[u][j].v];
                        if((dpForward[ux] | dpBack[vx]) == checkNeedPoint)
                            flag = 0;
                    }
                    if(flag)
                        usefulEdge.reset(forwardEdgeId[u][i].id);
                }
                for(int i = 1;i < backEdgeId[u].size(); i++){
                    if(usefulEdge.test(backEdgeId[u][i].id) == false)
                        continue;
                    int vx = tarbelong[backEdgeId[u][i].v];
                    if(dpBack[vx] == checkNeedPoint) continue;
                    int flag = 1;
                    for(int j = 0;j < forwardEdgeId[u].size(); j++){
                        if(usefulEdge.test(forwardEdgeId[u][j].id) == false)
                            continue;
                        int ux = tarbelongback[forwardEdgeId[u][j].v];
                        if((dpForward[ux] | dpBack[vx]) == checkNeedPoint)
                            flag = 0;
                    }
                    if(flag)
                        usefulEdge.reset(backEdgeId[u][i].id);
                }
            }
            int tarMark,newid = 0;
            vector<int> contain[maxV],tarstack;
            int tarbelong[maxV],tarbelongback[maxV],tarfloor[maxV],tarCheck[maxV];
            void tarJan(int u,int f,vector<Node>edge[]){
                tarfloor[u] = tarCheck[u] = ++tarMark;
                tarstack.push_back(u);
                check[u] = 1;
                for(int i = 0;i < edge[u].size(); i++){
                    if(usefulEdge[edge[u][i].id] == false) continue;
                    checkEdge.reset(edge[u][i].id);
                    int v = edge[u][i].v;
                    if(v == f) continue;
                    if(check[v] == 1) tarfloor[u] = min(tarfloor[u],tarfloor[v]);
                    else if(check[v] == 0) {
                        tarJan(v,f,edge);
                        tarfloor[u] = min(tarfloor[u],tarfloor[v]);
                    }
                }
                if(tarCheck[u] == tarfloor[u]){
                    contain[newid].clear();
                    int v;
                    do{
                        v = tarstack.back();
                        tarstack.pop_back();
                        tarbelong[v] = newid;
                        contain[newid].push_back(v);
                        check[v] = 2;
                    }while(u != v);
                    newid++;
                }
            }

            void updateEdgestate(int u,bitset<maxNeedPointNum> dp[],vector<Node>edge[]){
                if(checkVertex.test(u)) return;
                checkVertex.set(u);
                dp[u].reset();
                for(int i = 0;i < contain[u].size() ;i++){
                    int v = contain[u][i];
                    if(needPoint.test(v))
                        dp[u].set(mapToId[v]);
                    for(int j = 0;j < edge[v].size(); j++){
                        int id = edge[v][j].id;
                        int vv = edge[v][j].v;
                        if(usefulEdge.test(id) && tarbelong[vv] != u){
                            updateEdgestate(tarbelong[vv],dp,edge);
                            dp[u] |= dp[tarbelong[vv]];
                        }
                    }
                }
            }

            //获取能访问到的点
            void countVisitNeed(int s,int u,vector<Node> edge[],bitset<maxNeedPointNum>& get){
                if(s == u) return ;
                //cout<<s<<" "<<u<<" ";
                checkVertex.reset();
                int top = 0, tail = 0;
                if(needPoint.test(s))
                    get.set(mapToId[s]);
                que[top++] = s;
                checkVertex.set(s);

                while(tail < top){
                    s = que[tail++];
                    for(int i = 0;i < edge[s].size() ;i++){
                        if(edge[s][i].v == u) continue;
                        checkEdge.reset(edge[s][i].id);
                        if(checkVertex.test(edge[s][i].v) == false && usefulEdge.test(edge[s][i].id)){
                            que[top++] = edge[s][i].v;
                            checkVertex.set(edge[s][i].v);
                            if(needPoint.test(edge[s][i].v))
                                get.set(mapToId[edge[s][i].v]);
                        }
                    }
                }
                //cout<<tail<<endl;
            }

            void init(int Time,int tp){
                mark = 1;
                memset(markM,0,sizeof(markM));
                type = tp;
                //resultPath.clear();
                startTime = getSystemTime();
                TimeLong = Time;
                route.clear();
                covernum = needVnum - 1;
                maxCost = 1000000;
                memset(check,0,sizeof(check));
                check[startV] = 1;
                ansSize = 0;
                int EendT = getSystemTime() + TimeLong;
                while(getSystemTime() < EendT){
                    if(resultPath.size() > 100) break;
                    startTime = getSystemTime();
                    TimeLong = 0.5 * CLOCKS_PER_SEC;
                    //cout<<"begin"<<endl;
                    dfs_A(startV,0,10);
                    //cout<<"next "<<" "<<TimeLong<<" "<<getSystemTime() - startTime<<" "<<resultPath.size()<<endl;
                }
            }
            void init0(int Time,int tp){
                mark = 1;
                memset(markM,0,sizeof(markM));
                type = tp;
                //resultPath.clear();
                startTime = getSystemTime();
                TimeLong = Time;
                route.clear();
                covernum = needVnum - 1;
                maxCost = 1000000;
                memset(check,0,sizeof(check));
                check[startV] = 1;
                ansSize = 0;
                dfs_A(startV,0,0);
            }

            struct BEdge{
                int u,dist;
                bool operator < (const BEdge b)const{
                    if(dist == b.dist) return u > b.u;
                    return dist < b.dist;
                }
            };
            int dist[maxV];
            int ansSize;
            void dfs_A(int u,int cost,int lessN){
                if(resultPath.size() > 100) return;
                int flagAns = resultPath.size();
                endTime = getSystemTime();
                int overSize = ansSize;
                if(endTime - startTime > TimeLong ) return ;
               // if(cost >= maxCost ) return ;
                if(u == endV && covernum == 0 ){
                    static int ansnumber = 0;
                    //cout<<"get:"<<type<<"::"<<ansnumber++<<endl;
                    resultPath.push_back(route);
                    maxCost = cost;
                    //cout<<"time:"<<getSystemTime()-startTime<<" new anser "<<maxCost<<endl;
                    return ;
                }
                if(u == endV) return ;
                if(needPoint[u]) ansSize++;
                for(int i = 0 ;i < edgeTo[u].size() ;i++){
                    mark++;
                    edgeTo[u][i].dist = dfs_cut(edgeTo[u][i].v);
                    if(edgeTo[u][i].dist == covernum) edgeTo[u][i].dist = 1;
                    else edgeTo[u][i].dist = -1;
                }

                for(int i = 0;i < edgeTo[u].size() ;i++){
                    if(edgeTo[u][i].dist == 1){
                        mark++;
                        edgeTo[u][i].dist = bfs_find(edgeTo[u][i].v);
                    }
                }
                //cout<<"hehe"<<endl;
                sort(edgeTo[u].begin(),edgeTo[u].end());
                for(int i = 0;i < edgeTo[u].size() ; i++){
                    if(lessN > 0) i = rand() % edgeTo[u].size();
                    if(edgeTo[u][i].dist != -1){
                        int v = edgeTo[u][i].v;
                        check[v] = 1;
                        route.push_back(edgeTo[u][i].id);
                        if(needPoint[v]) covernum--;
                        dfs_A(v,cost+edgeTo[u][i].c,lessN-1);
                        if(needPoint[v]) covernum++;
                        route.pop_back();
                        check[v] = 0;
                    }
                    if(lessN >0) return ;
                    if(flagAns  < resultPath.size()) break;
                }
                //cout<<"haha"<<endl;
            }
            int dfs_cut(int u){
                if(markM[u] == mark || check[u]) return 0;
                int ans = 0;
                markM[u] = mark;
                if(needPoint[u] ) ans++;
                if(u == endV) return ans;
                for(int i = 0;i < edgeTo[u].size(); i++){
                    ans += dfs_cut(edgeTo[u][i].v);
                }
                return ans;
            }
            int bfs_find(int u){
                if(needPoint[u]) return 0;
                if(type == 0){
                memset(dist,0x3f,sizeof(dist));
                set<BEdge> haha;
                BEdge x,y;
                x.u = u;
                x.dist = 0;
                dist[u] = 0;
                haha.insert(x);
                while(haha.size() > 0){
                    x = *haha.begin();
                    haha.erase(haha.begin());
                    if(x.dist > dist[x.u]) continue;
                    for(int i = 0;i < edgeTo[x.u].size(); i++){
                        int v = edgeTo[x.u][i].v;
                        if(check[v]) continue;
                        if(v == endV && covernum != 1) continue;
                        if(needPoint[v]) return x.dist + edgeTo[x.u][i].c;
                        y.u = v;
                        y.dist = x.dist + edgeTo[x.u][i].c;
                        if(y.dist < dist[v]){
                            dist[v] = y.dist;
                            haha.insert(y);
                        }
                    }
                }
                    return -1;
                }


                int top = 1,tail = 0;
                queue[0] = u;
                markM[u] = mark;
                from[u] = 0;
                while(tail < top){
                    u = queue[tail++];
                    for(int i = 0;i < edgeTo[u].size(); i++){
                        int v = edgeTo[u][i].v;
                        if(check[v] || markM[v] == mark) continue;
                        if(v == endV && covernum != 1) continue;
                        if(needPoint[v]) return from[u]+1;
                        queue[top++] = v;
                        from[v] = from[u]+1;
                        markM[v] = mark;
                    }
                }
                return -1;
            }

            void ans_check(vector<int> ansPath){
                if(startV != wholeEdge[ansPath[0]].u) printf("Wrong at begin vertex!\n");
                if(endV != wholeEdge[ansPath[ansPath.size()-1]].v) printf("Wrong at end vertex!\n");
                bitset<maxNeedPointNum> xc;
                xc.reset();
                for(int i = 0;i < ansPath.size(); i++){
                    if(needPoint.test(wholeEdge[ansPath[i]].v))
                        xc.set(mapToId[wholeEdge[ansPath[i]].v]);
                    if(needPoint.test(wholeEdge[ansPath[i]].u))
                        xc.set(mapToId[wholeEdge[ansPath[i]].u]);
                    if(ansPath[i] < 0 || ansPath[i] > edgeNum) printf("Wrong link ID at %d!\n",ansPath[i]);
                    else {
                        if(i > 0){
                            if(wholeEdge[ansPath[i]].u != wholeEdge[ansPath[i-1]].v)
                                printf("The adjacent edge without same vertex at [%d,%d]!\n",wholeEdge[ansPath[i-1]].v,wholeEdge[ansPath[i]].u );
                        }
                    }
                    for(int j = 0;j < i; j++){
                        if(ansPath[i] == ansPath[j])
                            printf("repass link [%d]!\n",ansPath[i]);
                    }
                }
                if(xc != checkNeedPoint) cout<<"wrong anser"<<endl;
                //cout<<"check over"<<endl;
            }
    };

    void init(){
        wholeEdge.clear();
        for(int i = 0;i < maxV ;i++)
            nodeEdge[i].clear();
    }
    void search_route(char *topo[MAX_EDGE_NUM], int edge_num, char *demand[MAX_DEMAND_NUM], int demand_num){
        int beginTime = getSystemTime();
        cout<<"begintime: "<<beginTime<<endl;
         //================================================================================================== Demand
        for(int i = 0;i < demand_num; i++){
            stringAnalysis(demand[i],needVertex[i]);
            needVertex[i].erase(needVertex[i].begin());
            //for(int j = 0;j < needVertex[i].size(); j++)
                //cout<<needVertex[i][j]<<" ";cout<<endl;
        }
        //================================================================================================== Edge
        vector<int> cluster;
        vertexNum = 0;
        set<int> getNode;
        occurVertex.reset();
        edgeNum = edge_num;
        for(int i = 0;i < edge_num; i++){
            stringAnalysis(topo[i],sourceEdge[i]);
            Edge newEdge(sourceEdge[i]);
            int u = newEdge.u,flag = 0;
            getNode.insert(newEdge.u);
            getNode.insert(newEdge.v);
            vertexNum = max(vertexNum,max(newEdge.u,newEdge.v));
            occurVertex.set(newEdge.u);
            occurVertex.set(newEdge.v);
            if(newEdge.u == needVertex[0][1]) continue;
            if(newEdge.v == needVertex[0][0]) continue;
            if(newEdge.u == newEdge.v) continue;
            for(int j = 0;j < nodeEdge[u].size();j++){
                if(wholeEdge[nodeEdge[u][j]].v == newEdge.v){
                    wholeEdge[nodeEdge[u][j]].edge.push_back(newEdge);
                    flag = 1;
                    break;
                }
            }
            if(flag == 0){
                WholeEdge we;
                we.v = newEdge.v;
                we.u = newEdge.u;
                we.edge.push_back(newEdge);
                nodeEdge[newEdge.u].push_back(wholeEdge.size());
                wholeEdge.push_back(we);
            }
        }
        edgeNum = wholeEdge.size();
        for(int i = 0;i < edgeNum ;i++){
            sort(wholeEdge[i].edge.begin(),wholeEdge[i].edge.end());
            wholeEdge[i].c[0] = wholeEdge[i].edge[0].c;
            if(wholeEdge[i].edge.size() > 1)
                wholeEdge[i].c[1] = wholeEdge[i].edge[1].c;
            else
                wholeEdge[i].c[1] = maxEdgeCost;
        }

        //================================================================================================== Information about map
        printf("Total edge: %d   Total vertex: %d \n",edge_num,getNode.size());
        printf("Max vertex number: %d  Left edge number: %d\n",vertexNum,edgeNum);

        Search work1;
        work1.initGraph(needVertex[0],0.5*CLOCKS_PER_SEC);
        //cout<<getSystemTime()-beginTime<<" "<<wholeEdge.size() - work1.usefulEdge.count()<<endl;

        Search work2;
        work2.initGraph(needVertex[1],0.5*CLOCKS_PER_SEC);
        int leftTime = 9*CLOCKS_PER_SEC - getSystemTime() + beginTime;
        work1.init0(leftTime/2,0);
        work2.init0(leftTime/2,1);
        leftTime = 9*CLOCKS_PER_SEC - getSystemTime() + beginTime;
        //cout<<"left Time: "<<leftTime<<endl;
        work1.init(leftTime/2,0);
        work2.init(leftTime/2,1);
        //cout<<work1.resultPath.size()<< " "<<work2.resultPath.size();
        //cout<<"end"<<endl;

        getBestAnser(work1.resultPath,work2.resultPath);
        printf("Total cost time: %d ms\n",getSystemTime()-beginTime);
        return ;
         cout<<"ans check"<<endl;
        for(int i = 0;i < work1.resultPath.size();i++)
            work1.ans_check(work1.resultPath[i]);
        for(int i = 0;i < work2.resultPath.size();i++)
            work2.ans_check(work2.resultPath[i]);
    }

    void getBestAnser(vector<vector<int> >&a,vector<vector<int> >&b){
        int ansA = 0, ansB = 0;
        set<int> result1[a.size()];
        for(int i = 0;i < a.size(); i++){
            for(int j = 0;j < a[i].size(); j++)
                result1[i].insert(a[i][j]);
        }

        int totalCost = maxEdgeCost * 2005, cost[2];
        for(int i = 0;i < a.size(); i++){

            vector<int>& x = a[i];
            int cost1 = 0;
            for(int j = 0;j < x.size(); j++)
                cost1 += wholeEdge[x[j]].c[0];
            for(int j = 0;j < b.size(); j++){
                vector<int>& y = b[j];
                int cost2 = 0;
                for(int k = 0;k < y.size(); k++){
                    if(result1[i].find(y[k]) != result1[i].end()){
                        cost2 += wholeEdge[y[k]].c[1];
                        if(wholeEdge[y[k]].edge.size() == 1)
                            cost2 += wholeEdge[y[k]].c[0];
                    }
                    else
                        cost2 += wholeEdge[y[k]].c[0];
                }
                if(cost2 + cost1 < totalCost){
                    totalCost = cost1 + cost2;
                    cost[0] = cost1;
                    cost[1] = cost2;
                    ansA = i;
                    ansB = j;
                }
            }
        }
        vector<int>& x = a[ansA];
        vector<int>& y = b[ansB];
        for(int i = 0;i < x.size(); i++)
            record_result(WORK_PATH, wholeEdge[x[i]].edge[0].id);
        for(int i = 0;i < y.size(); i++){
            if(result1[ansA].find(y[i]) != result1[ansA].end()){
                if(wholeEdge[y[i]].edge.size() == 1)
                    record_result(BACK_PATH, wholeEdge[y[i]].edge[0].id);
                else
                    record_result(BACK_PATH, wholeEdge[y[i]].edge[1].id);
            }
            else
                record_result(BACK_PATH, wholeEdge[y[i]].edge[0].id);
        }


        printf("Repeat link: %d   Path 1 Cost: %d  Path 2 Cost: %d Total cost: %d\n",totalCost/maxEdgeCost,cost[0],cost[1]%maxEdgeCost ,cost[0]+cost[1]%maxEdgeCost);
    }

    int getSystemTime(){
       return clock();
    }

    void stringAnalysis(char *word,vector<int>& result){
        result.clear();
        int len = strlen(word);
        int u = 0;
        for(int i = 0;i < len; i++){
            if(word[i] == 'N') return ;
            if(word[i] > '9' || word[i] < '0'){
                result.push_back(u);
                u = 0;
                continue;
            }
            u = word[i] - '0' + u * 10;
        }
        if(u != 0)result.push_back(u);
    }
}
void search_route(char *topo[MAX_EDGE_NUM], int edge_num, char *demand[MAX_DEMAND_NUM], int demand_num)
{
    GDRetop::search_route(topo,edge_num,demand,demand_num);
}
